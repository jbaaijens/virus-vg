# Virus-VG: viral haplotype reconstruction from contigs using variation graphs.

License: MIT (see LICENSE)

Current version: 0.0.1

### Motivation ###
Viruses populate their hosts as a viral quasispecies: a collection of genetically related mutant strains. Viral quasispecies assembly refers to reconstructing the strain-specific haplotypes from read data, and predicting their relative abundances within the mix of strains, an important step for various treatment-related reasons. Reference-genome-independent (*de novo*) approaches have yielded benefits over reference-guided approaches, because reference-induced biases can become overwhelming when dealing with divergent strains. While being very accurate, extant de novo methods only yield rather short contigs. Virus-VG aims to reconstruct full-length haplotypes together with their abundances from such contigs, represented as a genome variation graph.

### Installation and dependencies ###
[Download](https://bitbucket.org/jbaaijens/virus-vg/downloads/?tab=tags) the latest release of this Virus-VG repository. Let `/path/to/virus-vg/` denote the path to the virus-vg root directory.

All dependencies (except vg, see below) are listed in `conda_list.txt`. The recommended way to install these is using [(mini)conda](https://conda.io/miniconda.html), which installs all packages at once without any root privileges:
```
conda create --name virus-vg-deps --file /path/to/virus-vg/conda_list_explicit.txt
```
In addition, you need to
download [vg version 1.7.0](https://github.com/vgteam/vg/releases/download/v1.7.0/vg-v1.7.0) and make sure that you
have a [license for Gurobi](https://user.gurobi.com/download/licenses/free-academic) (free for academic use). Note that the Gurobi installation is already managed by conda.


### Input ###
Virus-VG requires as input a fasta file with pre-assembled contigs and two fastq files (forward and reverse) with the original sequencing data. For the assembly step, we recommend using [savage](https://bitbucket.org/jbaaijens/savage) because it produces highly accurate, strain-specific contigs.

### Output ###
Virus-VG reconstructs the haplotypes present in the sample, along with their relative abundance rates. It outputs a fasta file with the resulting haplotypes, the corresponding frequencies added to the identifier field. In addition, the final genome variation graph is written to a GFA file.

### Workflow ###
Activate your conda virtual environment:
```
source activate virus-vg-deps
```

**Step 1: variation graph construction**

Using `build_graph_msga.py` you build your contig variation graph: first it performs
multiple sequence alignment (MSA) using the [vg toolkit](https://github.com/vgteam/vg). The resulting graph is then transformed into a contig variation graph and node abundances are computed by mapping the original read set to the graph.
```
python /path/to/virus-vg/scripts/build_graph_msga.py -f <forward_fastq> -r <reverse_fastq> -c <contig_fasta> -vg /path/to/vg/executable -t <num_threads>
```
The graph and the node abundances are stored in `contig_graph.final.gfa` and    `node_abundances.txt' and used as input for step 2.
Note: this is the most time-consuming step in the workflow, because it requires read mapping to the contig variation graph. Allowing multithreading with `-t <num_threads>` can speed-up this step significantly.

**Step 2: build haplotypes**

This is the most interesting and also the most challenging step. Our current
approach is to list all possible s-t paths through the *contig variation
graph* which are supported by contigs. These s-t paths represent all possible
haplotypes in the sample.

Next, we assign relative abundance rates to each of the paths, while minimizing
the difference between node abundance and the sum of abundance rates of all
strains going through the node, summing over all nodes. This is formalized as
an optimization problem and solved using the LP solver in Gurobi.

This step is executed by running `optimize_strains.py`. The user needs to specify the threshold values `-m`, the minimal node abundance, and `-c`, the minimal strain abundance. We recommend setting -m=0.005*sequencing depth and -c=0.01*sequencing depth. For example, with an average sequencing depth of 20,000x, set -m=100 and -c=200.
```
python /path/to/virus-vg/scripts/optimize_strains.py -m <node_ab> -c <strain_ab> node_abundance.txt contig_graph.final.gfa
```
The final haplotypes are written to `haps.final.fasta` and the genome variation graph to `genome_graph.gfa`.

The conda environment can be deactivated by typing `source deactivate`.


### Example ###
The example directory contains a small example to test Virus-VG before applying it to your own data. The example data consists of a small set of contigs `input.fasta`, and two read files `forward.fastq` and `reverse.fastq`, corresponding to a sequencing depth of 200x.

To run the example, enter the example directory, activate the conda environment, and execute the following shell commands:
```
python ../scripts/build_graph_msga.py -f forward.fastq -r reverse.fastq -c input.fasta -vg /path/to/vg/executable -t 8
python ../scripts/optimize_strains.py -m 1 -c 2 node_abundance.txt contig_graph.final.gfa
```
Note that we use `-m=1` and `-c=2` because of the total sequencing depth of 200x. Please make sure to adjust these parameter settings to your own data as described in **step 2**.

The output of this example consists of 2 full-length (9292bp) haplotypes with a frequency of 0.537 and 0.463, respectively.


### Contact ###

In case of any questions or issues, please contact Jasmijn Baaijens:
<lastname> AT cwi DOT nl
